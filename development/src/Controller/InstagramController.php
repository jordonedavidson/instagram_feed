<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InstagramController extends AbstractController
{
    /**
     * @Route("/instagram", name="instagram")
     * 
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('instagram/index.html.twig', []);
    }

    /**
     * @Route("/admin", name="admin")
     * 
     * @return Response
     */
    public function admin(): Response
    {
        return $this->render('instagram/admin.html.twig', [
            'app_id' => $this->getParameter('app.id'),
        ]);
    }

    /**
     * @Route("/auth", name="authorise")
     * 
     * @param Request $request The request parameters sent to the route.
     * 
     * @return Response 
     */
    public function authoriseInstagram(Request $request) : Response
    {
        // Code is provided by instagram as a query parameter.
        $code = $request->query->get('code');

        if ($code) {
            // Use the code to get the access token from instagram.
            $client = new HttpClient();

            try {
                $result = $client->request(
                    'POST',
                    'https://api.instagram.com/oauth/access_token',
                    [
                        'body' => [
                            'client_id' => $this->getParameter('app.id'),
                            'client_secret' => $this->getParameter('app.secret'),
                            'grant_type' => 'authorization_code',
                            'redirect_uri' => $this->generateUrl('authorise', [], true),
                            'code' => $code,
                        ],
                    ]
                );

                // Save the access-token.
            } catch (\Exception $e) {

            }

            $this->redirectToRoute('instagram');
        }
    }
}
